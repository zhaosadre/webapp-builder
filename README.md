WebApp builder
==============

Scope
-----
This [Docker](http://docker.io) image contains the tools we use at [OSIMIS](http://www.osimis.io) to build our dynamic websites (ruby, node, gulp & bower).


Usage examples
--------------

```
sudo docker run -v $(pwd):/project osimis/webapp-builder /bin/bash -c "cd /project/apps/webApp && ./superbuild.sh"

```

It is very important to map your volume 'high enough' in case your build script is doing suffs like `cd ../../`.  Inside a container, you can not go upon the mount point.
